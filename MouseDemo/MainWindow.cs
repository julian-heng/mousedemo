using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using static MouseDemo.NativeMethods;

namespace MouseDemo
{
    public partial class MainWindow : Form
    {
        private IntPtr _mouseHook;
        private MouseMessage? _lastClick = null;
        private bool _lastClickIsDouble = false;
        private Stopwatch _stopwatch = new Stopwatch();
        private HashSet<MouseMessage> _clickMessages = new HashSet<MouseMessage> { MouseMessage.WM_LBUTTONDOWN, MouseMessage.WM_RBUTTONDOWN };

        public MainWindow()
        {
            InitializeComponent();
        }

        private int OnMouseEvent(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode >= 0)
            {
                var mouse = (MSLLHOOKSTRUCT?)Marshal.PtrToStructure(lParam, typeof(MSLLHOOKSTRUCT));
                var msg = (MouseMessage)wParam;
                if (mouse != null)
                {
                    var isClick = _clickMessages.Contains(msg);
                    var doubleClick = isClick && !_lastClickIsDouble && _lastClick == msg && _stopwatch.ElapsedMilliseconds < 500;

                    var mouseMoveStr = $"({mouse?.Pt.X}, {mouse?.Pt.Y})";
                    var mouseClickStr = "";
                    if (msg == MouseMessage.WM_LBUTTONDOWN)
                    {
                        mouseClickStr = doubleClick ? $"Double Left {mouseMoveStr}" : $"Left {mouseMoveStr}";
                    }
                    if (msg == MouseMessage.WM_RBUTTONDOWN)
                    {
                        mouseClickStr = doubleClick ? $"Double Right {mouseMoveStr}" : $"Right {mouseMoveStr}";
                    }

                    MouseMoveListBox.Items.Insert(0, mouseMoveStr);
                    if (!string.IsNullOrEmpty(mouseClickStr))
                    {
                        MouseClickListBox.Items.Insert(0, mouseClickStr);
                    }

                    if (isClick)
                    {
                        _lastClick = msg;
                        _lastClickIsDouble = doubleClick;
                        _stopwatch.Restart();
                    }
                }
            }

            return CallNextHookEx(_mouseHook, nCode, wParam, lParam);
        }

        private void OnLoad(object sender, EventArgs e)
        {
            SetupMouseHook();
        }

        private void OnClosed(object sender, FormClosedEventArgs e)
        {
            ClearMouseHook();
        }

        private void SetupMouseHook()
        {
            Debug.WriteLine("Hooking to mouse events...");

            _mouseHook = SetWindowsHookEx(
                HookType.WH_MOUSE_LL,
                OnMouseEvent,
                Marshal.GetHINSTANCE(Assembly.GetExecutingAssembly().GetModules()[0]),
                0
            );

            Debug.WriteLine("Successfully hooked to mouse events");
        }

        private void ClearMouseHook()
        {
            Debug.WriteLine("Clearing mouse hook...");

            if (_mouseHook != IntPtr.Zero)
            {
                if (!UnhookWindowsHookEx(_mouseHook))
                {
                    throw new Win32Exception("Unable to clear mouse hook.");
                }

                _mouseHook = IntPtr.Zero;
            }

            Debug.WriteLine("Successfully cleared mouse hook.");
        }
    }
}