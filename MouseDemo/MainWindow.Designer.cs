﻿namespace MouseDemo
{
    partial class MainWindow
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.MouseMoveListBox = new System.Windows.Forms.ListBox();
            this.MouseClickListBox = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.MouseMoveListBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.MouseClickListBox);
            this.splitContainer1.Size = new System.Drawing.Size(800, 450);
            this.splitContainer1.SplitterDistance = 266;
            this.splitContainer1.TabIndex = 0;
            // 
            // MouseMoveListBox
            // 
            this.MouseMoveListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MouseMoveListBox.FormattingEnabled = true;
            this.MouseMoveListBox.ItemHeight = 15;
            this.MouseMoveListBox.Location = new System.Drawing.Point(0, 0);
            this.MouseMoveListBox.Margin = new System.Windows.Forms.Padding(12);
            this.MouseMoveListBox.Name = "MouseMoveListBox";
            this.MouseMoveListBox.Size = new System.Drawing.Size(266, 450);
            this.MouseMoveListBox.TabIndex = 0;
            // 
            // MouseClickListBox
            // 
            this.MouseClickListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MouseClickListBox.FormattingEnabled = true;
            this.MouseClickListBox.ItemHeight = 15;
            this.MouseClickListBox.Location = new System.Drawing.Point(0, 0);
            this.MouseClickListBox.Margin = new System.Windows.Forms.Padding(12);
            this.MouseClickListBox.Name = "MouseClickListBox";
            this.MouseClickListBox.Size = new System.Drawing.Size(530, 450);
            this.MouseClickListBox.TabIndex = 1;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "MainWindow";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OnClosed);
            this.Load += new System.EventHandler(this.OnLoad);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private SplitContainer splitContainer1;
        private ListBox MouseMoveListBox;
        private ListBox MouseClickListBox;
    }
}